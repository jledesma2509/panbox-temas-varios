package com.panbox.panbox

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.panbox.panbox.databinding.ItemPokemonBinding
import com.panbox.panbox.model.Pokemon

class PokemonAdapter(var pokemons : MutableList<Pokemon> = mutableListOf()) : RecyclerView.Adapter<PokemonAdapter.PokemonAdapterViewHolder>() {


    //ViewHolder  //XML y DATA
    //itemView = item_pokemon.xml
    inner class PokemonAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private val binding : ItemPokemonBinding = ItemPokemonBinding.bind(itemView)

        fun bind(pokemon: Pokemon){

            binding.tvNombre.text = pokemon.nombre

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonAdapterViewHolder {

        //view = item_pokemon.xml
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon,parent,false)
        return PokemonAdapterViewHolder(view)

    }

    fun updateList(pokemons: MutableList<Pokemon>){
        this.pokemons = pokemons
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return pokemons.size
    }

    override fun onBindViewHolder(holder: PokemonAdapterViewHolder, position: Int) {

        val pokemon = pokemons[position]
        holder.bind(pokemon)

    }

}