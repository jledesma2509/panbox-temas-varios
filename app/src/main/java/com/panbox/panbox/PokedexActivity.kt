package com.panbox.panbox

import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.panbox.panbox.databinding.ActivityMainBinding
import com.panbox.panbox.databinding.ActivityPokedexBinding
import com.panbox.panbox.model.Pokemon

class PokedexActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPokedexBinding

    private lateinit var adaptador : PokemonAdapter
    private var pokemons : MutableList<Pokemon> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPokedexBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.progress.visibility = View.VISIBLE

        setupAdapter()
        loadData()


    }

    private fun loadData() {

        Thread.sleep(4000)
        //API REST
        pokemons.add(Pokemon("Balbusour",""))
        pokemons.add(Pokemon("Charmander",""))
        pokemons.add(Pokemon("Balbusour",""))
        pokemons.add(Pokemon("Charmander",""))
        pokemons.add(Pokemon("Balbusour",""))
        pokemons.add(Pokemon("Charmander",""))
        pokemons.add(Pokemon("Balbusour",""))
        pokemons.add(Pokemon("Charmander",""))
        pokemons.add(Pokemon("Balbusour",""))
        pokemons.add(Pokemon("Charmander",""))

        adaptador.updateList(pokemons)

        binding.progress.visibility = View.GONE

    }

    private fun setupAdapter() {

        adaptador = PokemonAdapter()
        binding.rvPokedex.adapter = adaptador
        binding.rvPokedex.layoutManager = LinearLayoutManager(this)
        //binding.rvPokedex.layoutManager = GridLayoutManager(this,3)

    }
}