package com.panbox.panbox.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.panbox.panbox.R
import com.panbox.panbox.databinding.FragmentOpcionUnoBinding


class OpcionUnoFragment : Fragment() {

    private lateinit var binding : FragmentOpcionUnoBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_opcion_uno, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentOpcionUnoBinding.bind(view)


    }


}