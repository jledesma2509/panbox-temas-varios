package com.panbox.panbox

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.panbox.panbox.databinding.ActivityMainBinding
import com.panbox.panbox.fragment.OpcionDosFragment
import com.panbox.panbox.fragment.OpcionUnoFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private lateinit var fragment : Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnOpcionUno.setOnClickListener {

            fragment = OpcionUnoFragment()
            insertarFragmento()
        }

        binding.btnOpcionDos.setOnClickListener {

            fragment = OpcionDosFragment()
            insertarFragmento()
        }
    }

    fun insertarFragmento(){

        fragment?.let {
            supportFragmentManager.beginTransaction().replace(R.id.contenedor,it).commit()
        }
    }
}